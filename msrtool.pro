# Copyright (c) 2011-2013 by the original author and all contributors
# of this file. All rights reserved.
#
# Author: Feng Zhang (zhjinf@gmail.com)
# Date  : 2013-11-04
# Note  : This work is a part of my research during my Ph.D study at
#         Software Reenginnering Lab of Queen\'s University at
#         Kingston, Ontario, Canada.
#
# History:
#    (Format: Version, Date, Developer, Comments)
#    1.0, 2013-11-04, Feng Zhang (zhjinf@gmail.com), Add this header.
#

QT       += core gui network xml xmlpatterns

TARGET = msrtool
TEMPLATE = app

LIBS += -L./lib -lkyotocabinet -lphoenix
INCLUDEPATH += ./include ./include/kyotocabinet

SOURCES += src/main.cpp\
	src/fzkcdb.cpp

HEADERS  += include/msrtool.h \
	include/fzkcdb.h \
	include/phoenix++/atomic.h \
	include/phoenix++/combiner.h \
	include/phoenix++/container.h \
	include/phoenix++/locality.h \
	include/phoenix++/map_reduce.h \
	include/phoenix++/processor.h \
	include/phoenix++/scheduler.h \
	include/phoenix++/stddefines.h \
	include/phoenix++/synch.h \
	include/phoenix++/task_queue.h \
	include/phoenix++/thread_pool.h
