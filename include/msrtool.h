/*
* Copyright (c) 2011-2013 by the original author and all contributors
* of this file. All rights reserved.
*
* Author: Feng Zhang (zhjinf@gmail.com)
* Date  : 2013-11-04
* Note  : This work is a part of my research during my Ph.D study at
*         Software Reenginnering Lab of Queen's University at
*         Kingston, Ontario, Canada.
*
* History:
*    (Format: Version, Date, Developer, Comments)
*    1.0, 2013-11-04, Feng Zhang (zhjinf@gmail.com), Add this header.
*/

#ifndef _INCLUDE_MSRTOOL_H_
#define _INCLUDE_MSRTOOL_H_

#endif // #ifndef _INCLUDE_MSRTOOL_H_
