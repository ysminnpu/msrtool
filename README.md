Copyright (c) 2011-2012 by the original author and all contributors
of this file. All rights reserved.
Author: Feng Zhang (zhjinf@gmail.com)
Date  : 2013-11-04
Note  : This work is a part of my research during my Ph.D study at
        Software Reenginnering Lab of Queen's University at
        Kingston, Ontario, Canada.
History:
   (Format: Version, Date, Developer, Comments)
   1.0, 2013-11-04, Feng Zhang (zhjinf@gmail.com), Add this header.


# This project aims to create a scalable tool for mining software repositories.


